import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;

  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    this.selectedItem = navParams.get('item');



    this.items = [];
    this.storage.forEach(element => {
      this.items.push(element)
    });
  }


}
