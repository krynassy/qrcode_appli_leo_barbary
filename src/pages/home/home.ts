import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import QRCode from 'qrcode';


import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  code = 'some sample string';
  generated = '';

  displayQrCode() {
    return this.generated !== '';
  }

  constructor(public navCtrl: NavController,public storage: Storage, private socialSharing:SocialSharing) {
    storage.ready().then(() => {
    });
  }

 
}
